package grifts

import (
	"github.com/g-dormoy/gitlab_ci/actions"
	"github.com/gobuffalo/buffalo"
)

func init() {
	buffalo.Grifts(actions.App())
}
